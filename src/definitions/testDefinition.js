
const board = new Boards()
const board1  = new Boards()

board.boardMaxSize(1000, 1000) //
board.createBoard(1) //crea tipo de board 1
board.boardSize(1000,1000) //el tamaño del board
board.setGrid(true) //establece si se ve o no la regilla o cuadrilla
board.createLine(1, [-5,1],[5,1]) // crea una linea, el primer parametro es el dash, los siguiente son puntos de la linea [x,y]
board.createLine(1, [-5,-1],[5,-1])// crea una linea, el primer parametro es el dash, los siguiente son puntos de la linea [x,y]
board.setBourdingBox(-5,9,5,-9)//establece las dimensiones del board

board.createText('1', [-1,-0.2]) //crea un texto en determinada coordenada
board.createText('π/4', [-0.7,-0.2]) //crea un texto en determinada coordenada
board.createText('π/8', [0.01,-0.2])//crea un texto en determinada coordenada

board.createText('1', [-0.3,1.2],{fontWeight:700, fontSize:'16px'}) //crea un texto en determinada coordenada

board.createAsymptotes(1,-2.8, -1.7,-1.5, -1.4,-0.7 ,-0.3, 0.3, 0.7, 1.4,1.5,2.9) //crea lineas verticales en cada una de las coordenadas

board1.createBoard(1)
board1.boardSize(700,700)

board1.setGrid(false)
board1.createLine(1, [-5,1],[5,1])
board1.createLine(1, [-5,-1],[5,-1])
board1.setBourdingBox(-5,5,5,-5)

board1.createText('1', [-1,-0.2])
board1.createText('1', [-0.3,1.2],{fontWeight:700, fontSize:'16px'})

board1.createAsymptotes(1,-3.8, -2.7,-2.5, -2.3, -1.3, 1.3, 1.7, 2.3, 2.5, )


const board2 = new Boards()

board2.createBoard(1)
board2.createBoard(1) //crea tipo de board 1
board2.boardSize(1000,1000) //el tamaño del board

//board.setGrid(false)

//Eliezer

const artifact_0 = new ArtifactValidateInput()

artifact_0.createNewCurveToValidate("curve1")
artifact_0.createNewCurveToValidate("curve2")

//artifact_0.insertDefaultInputToCurve("x", "-x", "curve1")
//artifact_0.insertDefaultInputToCurve("1", "-1", "curve1")
artifact_0.setMaxCurves(20)

artifact_0.insertInputToCurve('-3', '-3', 'curve1')
artifact_0.insertInputToCurve('-2', '-3', 'curve1')
artifact_0.insertOpenPointToCurve('-2','-2', 'curve2')
artifact_0.insertInputToCurve('-1', '-2', 'curve2')


//artifact_0.insertInfinitiesToCurve([7,8,9],[1,2],"curve1")
//artifact_0.insertInputToCurve('2', '-2', 'curve1')
/* artifact_0.insertInputToCurve('3', '-3', 'curve1')
artifact_0.insertInputToCurve('0', '0', 'curve1')
artifact_0.insertInputToCurve('-1', '1', 'curve1')
artifact_0.insertInputToCurve('-2', '2', 'curve1') */
//artifact_0.insertInputToCurve('-3', '3', 'curve1')
//artifact_0.addPointWithInputs([1,'1'])


//artifact_0.createNewCurveToValidate("curve2")

//artifact_0.insertDefaultPointsToCurves('curve1',[-3,3],[-3.5,3],[3,-4]) //CREA PUNTOSP OR DEFECTOS ************ ESTA ES ESSS **************
//artifact_0.insertInfinitiesToCurve([1,2,3],[4,5,6],"curve1")

artifact_0.addHelpMessage("este es el titulo", 'este es el texto')

//Jhorman 
const exerciseTable = new ArtifactKeysAndTables()

exerciseTable.createTable() //CREA LA TABLA
exerciseTable.addRowinTable('Tecla', ["2",'3']) //CREA UNA FILA
exerciseTable.addRowinTable('Tecla2', "123456")//CREA UNA FILA

exerciseTable.addRowinTable('Tecla', "2")//CREA UNA FILA
exerciseTable.addRowinTable('Notación Funcional', 'f(x)', false, 'f(x)')//CREA UNA FILA


const ejercicio2 = new ArtifactKeysAndTables()

ejercicio2.createArtifactGridSimple()
ejercicio2.createRounded([2,3], 'hola')
//ejercicio2.createRoundedDefault('n')

/* 
ejercicio2.createKey('text1', 2)
ejercicio2.createRoundedDefault('n')

 */





const keyDefinition = new ArtifactKeysAndTables()
//keyDefinition.createRounded(2, '1')

/* exerciseTable.addRowinTable('Notación Funcional', 'f(x)', true, 'f(x)')
exerciseTable.addRowinTable('Ecuación en dos variables', 'y=', true, 'y=')
exerciseTable.addRowinTable('Por pares ordenados', '(x, y)', true, '(x,y)')
 */
//const keyDefinition = new ArtifactKeysAndTables()

keyDefinition.createArtifactGridSimple()
keyDefinition.createRoundedDefault('n')
keyDefinition.createDefaultKey('-( )')
keyDefinition.createRoundedDefault('-n')
keyDefinition.createKey('text1',2)
keyDefinition.createRounded([1])

const artifactTest = new ArtifactOwner()

artifactTest.createNewNode(11, 'node1', "#oneContainer") //se crea el nodo
artifactTest.createQuestionsGroup('node1', 'grupo1') //se crea el questionGroup
artifactTest.createOneQuestionWithManyInputs('pregunta para responder', false, ['Dominio', ['2','3']], ['Rango', 2]) //se define la pregunta
artifactTest.createSelectWithOneQuestion('pregunta del select', 'pregunta1', 'pregunta 2', ['pregunta 3', true], 'pregunta 4',)

artifactTest.createOneQuestionWithManyInputs('', true, ['Si xE(0,1], 1/xE?', ['2','3']], ['puntos de corte con el eje y', 2])

/* 
artifactTest.createSelectWithOneQuestion('pregunta del select','pregunta1','pregunta 2',['pregunta 3', true], 'pregunta 4', )
artifactTest.createRadioButton('preguntas del radioButton',['sí',true],'no') */

//Raul
const questions = new ArtifactOwner()

questions.createNewNode(11, 'node2', '.secondContainer')
questions.createQuestionsGroup('node2', 'z')

questions.createOneQuestionWithManyInputs('Maximos y minimos', false, ['Máx. Abs', '2'], ['Alcanzado en', 2])
questions.createOneQuestionWithManyInputs('', false, ['Máx Rel', '2'], ['Alcanzado en', 2])


const questions2 = new ArtifactOwner()
questions2.createNewNode(11, 'node3', '.thirdContainer')
questions2.createQuestionsGroup('node3')
questions2.createOneQuestionWithManyInputs('', false, ['Zonas de crecimiento', '2'])
questions2.createOneQuestionWithManyInputs('', false, ['Zonas de decrecimiento', '2'])



/* 
artifactTest.createNewNode(13,'node2','#tablaNumero1')
artifactTest.createTable('node2','y',)

artifactTest.createHeadOfTable('Opcion','tebajo','ssadasdasaaaaaaaaaaaaaaaaaaaaaa   ')

const textToRow = artifactTest.createTextFieldToTable('holaa')
const selectToRow = artifactTest.createSelectFieldToTable('1',['2'])
const input = artifactTest.createInputsToTable(1)

artifactTest.addRowToTable(textToRow,textToRow,textToRow,textToRow)
artifactTest.createNewNode(13,'node3', '#tabla2')
artifactTest.createTable('node3','y')
artifactTest.createHeadOfTable('Opcion','tebajo','ssadasdas','sadawewaaaaaaaaaaaaaaaead')

 */


//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::|| objetos de que recopilan las definiciones ||::::::::::::::::::::::::::::::::::::::::::::


const definitioBoards = {
    board_0: board.builder(),
    board_1: board1.builder(),
    board_2: board2.builder()
}

const definitionArtifacts = {
    //artifact_0: artifact_0.builder()
    artifact_0:{
        helpMsg: {
           title: 'titulo',
           text: 'esto es un texto de prueba',
        },
        maxCurves: 2,
        conditions: {
     
             pointsWithInputs:[
              {
               coord:1,
               value:'1'
              },
              {
                 coord:2,
                 value:'2'
              },
              {
                 coord:3,
                 value:'3'
              }
            ], 
     
           inputsToValidate: [
     
              {
                 inputs: [
                    {
                       defaultXvalue: '-2',
                       succesValue: '-2'
                    },
     
                       {
                          defaultXvalue: '-1',                  
                          succesValue: '-1',
        
                       },
                       {
                          defaultXvalue: '0',
                        
                          defaultYvalue:null,
                          succesValue: '0',
        
                       },
                    {
                       defaultXvalue: '1',
     
                       defaultYvalue: null,
                       succesValue: '1',
     
                    },
     
                 ],
                 //infinities: [[8, 9]],
                 pointDefault:[[2,-1],[3,-2]]
     
              },
     
              {
                 inputs: [
                    {
                       defaultXvalue: '-2',
                       succesValue: '-3'
                    },
     
                      {
                        defaultXvalue: '-1',                  
                        succesValue: '-3',
      
                     },
                     {
                        defaultXvalue: '0',
                      
                        defaultYvalue:null,
                        succesValue: '-3',
      
                     }, 
                    {
                       defaultXvalue: '1',
     
                       defaultYvalue: null,
                       succesValue: '-3',
     
                    },
     
                 ],
                // infinities: [[8, 9, 10]]
     
     
              }
           ]
     
        },
     },
}


const def = {
    artifact_1: exerciseTable.builder(),
    artifact_0: keyDefinition.builder()
}
/* 
const def2 = {
    artifact_0: keyDefinition.builder(),
    artifact_1: exerciseTable.builder(),
} */


const def1 = {
    artifact_0: artifactTest.builder(),
    artifact_1: questions.builder(),
    artifact_2: questions2.builder()
}


const cartesianArtifact = new ArtifactCartesian()
const example = new ArtifactCartesian()
//cartesianArtifact.addPointWithInputs([0.7,"\\frac{\\pi}{4}"],[2,"2"])
//cartesianArtifact.evaluateCurve('curve1', [-1.3,-3.6], [-0.3,-0.4 ],[0,0],[0.3,0.4], [0.7, 1], [1.3,3.6])
//cartesianArtifact.evaluateCurve('curve1', [-1.4,-5.7], [-0.7, -1],[-0.3,-0.4 ],[0,0],[0.3,0.4], [0.7, 1], [1.4,5.7])
cartesianArtifact.evaluateCurve('curve1', [1,1],[2,2])
cartesianArtifact.evaluateCurve('curve2', [-1,-1], [-2,-1])
cartesianArtifact.evaluateCurve('curve3', [-3,-3],[-4,3])

cartesianArtifact.setCurveholgure(1)
//cartesianArtifact.addInfinities('curve1',[6,7],[1,2,3])
//testArtifact.addInfinities('curve1',null, [1,2,3])


const testCartesian = {
   artifact_0: cartesianArtifact.builder(),
  // artifact_1: example.builder()
}

const aux = {
   artifact_0: artifact_0.builder()
}


generateArtifact(def) // motor para las tablas y las pantallas con screen
generator(def1) // motor para los formularios

mainOperation(definitioBoards, testCartesian); // motor de graficas normales
icMain(aux, definitioBoards) //motor para los boards con inputs

