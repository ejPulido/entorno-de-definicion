const $templateContainer = document.querySelector('#allTemplates')


/** Almacene los templates,
*
* Cree una variables con un template string `` que contenga el html que quiera agregar como templates
* 
*/


const templateInputCurves = `

<template id="tmp-inputCurves">
<div class="inputArtifact">
    <div class="inputArtifact__content">
     <div class="msg d-none">
         
     </div>
     <div class="inputArtifact__inputs">
          <!-- inputs -->
       </div>
       <div class="inputArtifact__board">
        <!-- board -->
       </div>
    </div>
    <div class="inputArtifact__btn btn-all">
      <button class="help-btn  buttonSecundary buttonKey d-none "></button>
      <button class="pointClose buttonSecundary buttonKey d-none" style="margin-left: 10px;" title="Punto para la curva"></button>
      <button class="infinite buttonSecundary  buttonKey" style="margin-left: 10px;"> </button>
      <button class="inputArtifact__btnReset  buttonSecundary buttonKey">Rehacer curva </button> 
      <button class="inputArtifact__btnCreateCurve buttonKey" >Crear curva </button>
    </div>
 </div>
</template>

<template id = "tmp-coords">
<div class="inputArtifact__coord">
  <span>(</span> <span class = 'defaultCoordX'>x</span> <span>,</span> <math-field class="inputArtifact__inputCoord" virtual-keyboard-mode='onfocus' keypress-sound = "none"></math-field><span>)</span>
</div>
</template>

`
const templateForms = `

<template id="globalTemplate">
     
<div id="artifact-container" class="d-flex w-100 flex-wrap justify-content-center rounded btn-all artifact-container">
<div id="content" class="s">

</div>
<div class="d-flex w-100 flex-wrap justify-content-center  rounded btn-all">
<div class="d-flex flex-row">
   <div class="d-flex flex-wrap justify-content-start"></div>

   <button class="reset button-marg buttonSecundary buttonKey" title="Reiniciar"></button>
   <button class="check button-marg buttonPrimary buttonKey" title="Validar"></button>

</div>
</div>

</div>

    </template>

    <template id="card-template">
    <div class="card" style="width:fit-content">
      <div class="card-header text-center"> Grafíca g</div>
        <div class="card-body">
        
        </div>
        <div class="defCartesian" data-board="board_1"></div>
        <div class="card-footer" >
  
        </div>
      </div>
    </template>

    <template id="tableTemplate">

    <table class="table table-bordered">
  
    </table>
  
    </template>

    <template id="simpleTemplateInput">
    <math-field  style =" width:100%;" >
    
    </math-field>          
    
    </template>
  
    <template id="simpleTemplateSelect">
    <div>
      <select class="form-select form-control" name="opciones" id="opciones"style="max-width:100%; max-height:100%; border:none; " >   


    </div>
  </select>
    </template>

    <template id="simpleTemplateRadio">
    <div class="form-check ml-3 mr-4 mt-1" style="display:inline-block !important; width:fit-content">
      <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
      <label class="form-check-label " for="flexRadioDefault1" >
        Test
      </label>
    </div>
    </template>

    <template id="template">
    <div id="artifact"></div>
    </template>
    <template id="templateInput">
      <div class="row">
        <div id="title" class="d-flex justify-content-start col-12 " >
        </div>
        <div id="content"class="d-flex justify-content-between col-12"></div>
        </div>
        </div>
     </template>

     <template id="templateSelect">
      <div class='row flex-wrap mt-2 d-flex' id="selectq" >
          <div class=" col-lg-6 col-md-12 col-sm-12">
            <label for="opciones"></label>
          </div>
          <div class="col-lg-6 col-md-12 col-sm-12  d-flex justify-content-start" >
            <select class="form-select form-control" name="opciones" id="opciones"style="width: fit-content ;max-width:90%;" >   
         </div>
         </select>
      </div>
   </template>

   <template id="templateCheck">
       
    <div class=" row d-flex mt-1" id="radioContainer" >
       <div class="title col-md-6  justify-content-start">
 
       </div>
       <div class="content col-6 justify-content-start">
 
       </div>
    </div>
 
 </template>

` 
const templateEngineOperation = `
<template id="tmp-SRP">
     <div id="artifacts" class="d-flex flex-column border-board-dark m-2 mt-5 squares-div">
      
        <div class="defRoadBoard jxgbox mb-1 boardResp" data-board="board_0" data-artifact="artifact_1">
         
        </div>
        
        <div class="d-flex w-100 flex-wrap  border border-dark rounded btn-all">
           <div class="d-flex flex-row">
              <div class="d-flex flex-column">
                 <div class="d-flex flex-wrap justify-content-center">
 
                    <button class="curve button-marg buttonTertiary buttonKey"
                       title="Crear curva con los puntos finales"></button>
                    <button class="pointClose button-marg buttonTertiary buttonKey"
                       title="Punto para la curva"></button>
                       <button class="pointInput button-marg buttonTertiary buttonKey"
                       title="Punto para la curva"></button>
                    <button class="btnAsymptotes button-marg buttonTertiary buttonKey" title="Asintota"></button>
                   <!-- <button class="btnGrid button-marg buttonTertiary buttonKey" title="Asintota"></button> -->
                     
                 </div>
                 <div class="d-flex flex-wrap">
                  <button class="menuCurvesShow buttonSecundary button-marg buttonKey"> </button>
                    <button class="help buttonSecundary button-marg  buttonKey" title="Ayuda"></button>
                    <button class="gear buttonSecundary button-marg  buttonKey"> </button>
                    <button class="infinite buttonSecundary button-marg  buttonKey"> </button>
                    <button class="back buttonSecundary button-marg  buttonKey" title="Retornar"></button>
                    <button class="reset button-marg buttonSecundary buttonKey" title="Reiniciar"></button>
                    <button class="check button-marg buttonPrimary buttonKey" title="Validar"></button>

                 </div>
              </div>
           </div>
        </div>
     </div>
  </template>
 
<template id="tmp-config">
 <div
    class="centerFloat pr-1 pl-1 toggleDisplay divConfig d-flex flex-row justify-content-left  align-items-center">
    <div class="configTop">
       <h5 style="margin-top: 10px; border-bottom: solid 2px black;">Configuraciones</h5>
       <button class="btnClose closed buttonKey button-marg"> </button>
    </div>
    <ul>
       <li>
          <p> Holgura de la curva <input class="rangeConfig" min="0.0" max="1" step="0.1" ; orient="vertical"
                type="range"> </p>
       </li>
       <li>
          <p> Color: <input class="colorConfig" type="color" name="" id=""> </p>
       </li>
    </ul>

 </div>
</template>
`

/* Almacene en este array los templates creados */
const TEMPLATES = [templateInputCurves, templateForms]


TEMPLATES.forEach(template =>$templateContainer.insertAdjacentHTML("afterbegin",template))